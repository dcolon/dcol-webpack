const path = require('path');
const glob = require('glob');
const fs   = require('fs')

const HTMLWebpackPlugin = require('html-webpack-plugin');

const generateHTMLPlugins = () => glob.sync('./src/pages/**/*.html').map(
    dir => {
        // Don't include shared if we have a .noshared file in the page folder.
        const chunks = fs.existsSync(path.dirname(dir) + '/.noshared') ? [path.basename(dir, '.html')] : ['shared', path.basename(dir, '.html')]
        // Inject bootstrap scripts in the head.
        let inject = path.basename(dir, '.html') === 'index' ? 'head' : 'true';
        // Rewrite all files to index.html in their folder except for index.
        let filename = path.basename(dir);//path.basename(dir, '.html') === 'index' ? 'index.html' : path.basename(dir, '.html') + '/index.html'
        return new HTMLWebpackPlugin({
            inject: inject,
            filename: filename,
            template: dir, // Input
            chunks: chunks,
        })
    }
);

const getEntries = () => {
    let result = {shared: './src/index.js'};
    const pages = glob.sync('./src/pages/*');
    pages.forEach(page => {
        if (fs.existsSync(page + '/index.js')) {
            result[path.basename(page)] = page + '/index.js';
        }
    });
    return result;
}

module.exports = {
    mode: 'development',
    devtool: 'eval-cheap-module-source-map',
    entry: getEntries(),
    devServer: {
        port: 8070,
        contentBase: path.join(__dirname, "dist"),
        historyApiFallback: {
            verbose: true,
            rewrites: [
                {
                    from: /^\/([a-z]+)$/,
                    to: function(context) {
                        return `/${context.match[1]}.html`;
                    }
                }
            ]
        },
    },
    node: {
        fs: 'empty'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env'],
                    plugins: [
                        ["prismjs", {
                            "languages": ["opencl"],
                            "plugins": ["line-numbers"],
                            "theme": "default",
                            "css": true
                        }]
                    ],
                }
            },
            {
                test: /\.html$/,
                loader: 'html-loader',
                options: {
                    attributes: true,
                    minimize: false,
                    preprocessor: (content, loaderContext) => {
                        let menuHtml = fs.readFileSync('./src/menu.html', "utf8");
                        let footerHtml = fs.readFileSync('./src/footer.html', "utf8");
                        // Activate correct mneu item.
                        const resource = path.basename(loaderContext.resource, '.html');
                        if (resource === 'index') {
                            menuHtml = menuHtml.replace('{{homeactive}}', 'active');
                            menuHtml = menuHtml.replace('{{projectsactive}}', '');
                        }
                        else {
                            menuHtml = menuHtml.replace('{{homeactive}}', '');
                            menuHtml = menuHtml.replace('{{projectsactive}}', 'active');
                        }
                        // Set year in footer.
                        footerHtml = footerHtml.replace('{{year}}', new Date().getFullYear()); 
                        // Then replace menu and footer tags in page html.
                        let result = content.replace('{{menu}}', menuHtml);
                        result     = result.replace('{{footer}}', footerHtml);
                        return result;
                    },
                }
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    {
                        // creates style nodes from JS strings
                        loader: "style-loader",
                    },
                    {
                        // translates CSS into CommonJS
                        loader: "css-loader",
                    },
                    {
                        // compiles Sass to CSS
                        loader: "sass-loader",
                    }
                    // Please note we are not running postcss here
                ]
            },
            {
                // Load all images as base64 encoding if they are smaller than 8192 bytes
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // On development we want to see where the file is coming from, hence we preserve the [path]
                            name: '[path][name].[ext]?hash=[hash:20]',
                            limit: 8192
                        }
                    }
                ]
            },
            {
                // Load all icons, fonts, and videos.
                test: /\.(eot|woff|woff2|svg|ttf|mp4|webm)([\?]?.*)$/,
                use: [
                    {
                        loader: 'file-loader',
                    }
                ]
            }
        ],
    },
    plugins: [
        ...generateHTMLPlugins(),
    ]
};
