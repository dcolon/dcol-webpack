import './style.css';
import iterate from './iterate';

function setup() {
  // Get the canvas.
  const canvas = setupCanvas();
  // get the context and scale to window size.
  const ctx  = canvas.getContext('2d');
  ctx.canvas.width  = window.innerWidth;
  ctx.canvas.height = window.innerHeight;
  // get the initial scale.
  const initialViewport = getInitialViewport(ctx.canvas.width, ctx.canvas.height);
  // Initial position.
  const initialPosition = {
    x: 0,
    y: 0,
  };
  // Set up button handling.
  document.onkeydown = (event) => {onKeyEvent(event, true)};
  document.onkeyup   = (event) => {onKeyEvent(event, false)};
  // Render initial frame.
  window.requestAnimationFrame((ts) => {
    draw(ctx, initialViewport, initialPosition)
  });
  return(
    {
      ctx: ctx,
      viewport: initialViewport,
      position: initialPosition,
      keyDown: {
        KeyW: false,
        KeyA: false,
        KeyS: false,
        KeyD: false,
        ArrowUp: false,
        ArrowDown: false
      }
    }
  );
}

function setupCanvas() {
  // Set up the canvas.
  const canvas = document.createElement('canvas');
  document.body.appendChild(canvas);
  return canvas;
}

function getInitialViewport(width, height) {
  const initialBound = 1;
  // Get <-2,2> in the smallest part (height or width)
  // Scale the other accordingly.
  if (width > height) {
    const rel = width / height;
    return {
      N: initialBound,
      S: -initialBound,
      E: rel * initialBound,
      W: rel * -initialBound
    };
  }
  else {
    const rel = height / width;
    return {
      N: rel * initialBound,
      S: rel * -initialBound,
      E: initialBound,
      W: -initialBound
    };
  }
}

function draw(ctx, viewport) {
  // Get the size of the canvas. (pixels)
  const widthP  = ctx.canvas.width;
  const heightP = ctx.canvas.height;
  // Calculate the width of the viewport
  const width  = viewport.E - viewport.W;
  const height = viewport.N - viewport.S;
  // Calculate the scale factors in x and y so that:
  // pixel * scale = coord
  const scaleX = width / widthP;
  const scaleY = height / heightP;
  // Create the imagedata
  let imageData = ctx.createImageData(widthP, heightP);
  // Iterate over pixels in y.
  for (let yp = 0; yp < heightP; yp++) {
    // Convert out pixels to coordinates from the viewport.
    // Shift by halfheight so our origin is in the center.
    let y = viewport.S + (yp * scaleY);
    // And in x.
    for (let xp = 0; xp < widthP; xp++) {
      // Calculate the pixel index for the imagedata.
      var i = (yp * widthP + xp) * 4;
      // Convert out pixels to coordinates form the bounds.
      // Shift by halfwidth so our origin is in the center.
      let x = viewport.W + (xp * scaleX);
      // Iterate over the mandelbrot set.
      let iterated = iterate(x, y, iterations);
      // Then draw the pixel in greyscale.
      imageData.data[i+0] = 255 - (iterated * resolution); // R.
      imageData.data[i+1] = 255 - (iterated * resolution); // G.
      imageData.data[i+2] = 255 - (iterated * resolution); // B.
      imageData.data[i+3] = 255;                           // Alpha.
    }
  }
  // Finally write it to the canvas.
  ctx.putImageData(imageData, 0, 0);
}

function onKeyEvent(event, isDown) {
  // Only run when we're ready.
  if (!initialised) {
    return;
  }
  // Do we need an update?
  let needDraw = false;
  let scaleX = 1;
  let scaleY = 1;
  // used in moving (so we move relative to scale).
  let xSize = 0;
  let ySize = 0;
  // Update the state information.
  if (applicationData.keyDown.hasOwnProperty(event.code)) {
    applicationData.keyDown[event.code] = isDown;
    // We're handling this.
    if (isDown) {
      switch(event.code) {
        case 'KeyW':
          ySize = Math.abs(applicationData.viewport.N - applicationData.viewport.S);
          applicationData.viewport.N -= positionStep * ySize;
          applicationData.viewport.S -= positionStep * ySize;
          needDraw = true;
          break;
        case 'KeyS':
          ySize = Math.abs(applicationData.viewport.N - applicationData.viewport.S);
          applicationData.viewport.N += positionStep * ySize;
          applicationData.viewport.S += positionStep * ySize;
          needDraw = true;
          break;
        case 'KeyD':
          xSize = Math.abs(applicationData.viewport.E - applicationData.viewport.W);
          applicationData.viewport.E += positionStep * xSize;
          applicationData.viewport.W += positionStep * xSize;
          needDraw = true;
          break;
        case 'KeyA':
          xSize = Math.abs(applicationData.viewport.E - applicationData.viewport.W);
          applicationData.viewport.E -= positionStep * xSize;
          applicationData.viewport.W -= positionStep * xSize;
          needDraw = true;
          break;
        case 'ArrowUp':
          scaleX = scaleStep * (applicationData.viewport.E - applicationData.viewport.W);
          scaleY = scaleStep * (applicationData.viewport.N - applicationData.viewport.S);
          applicationData.viewport.E -= scaleX;
          applicationData.viewport.W += scaleX;
          applicationData.viewport.N -= scaleY;
          applicationData.viewport.S += scaleY;
          needDraw = true;
          break;
        case 'ArrowDown':
          scaleX = -scaleStep * (applicationData.viewport.E - applicationData.viewport.W);
          scaleY = -scaleStep * (applicationData.viewport.N - applicationData.viewport.S);
          applicationData.viewport.E -= scaleX;
          applicationData.viewport.W += scaleX;
          applicationData.viewport.N -= scaleY;
          applicationData.viewport.S += scaleY;
          needDraw = true;
          break;
      }
    }
  }
  if (needDraw) {
    window.requestAnimationFrame((ts) => {
      draw(applicationData.ctx, applicationData.viewport)
    });
  }
}

// Set up.
let initialised = false;
// Power of 2! Lower is higher detail but slower.
const resolution = 1;
const iterations = 256 / resolution;
const positionStep = 0.1;
const scaleStep = 0.1;
let applicationData;
applicationData = setup();
initialised = true;