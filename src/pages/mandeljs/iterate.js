/**
 * Iterates over the mandelbrot function.
 * Returns the number of iterations before series >= 2
 * Returns maxIterations if series does not surpass 2
 * before maxIterations is reached.
 * @param {float} cReal
 *    Coordinate along real axis.
 * @param {float} cImag
 *    Coordinate along the imaginary axis.
 * @param {int} maxIterations
 *    Max iterations before returning.
 */
function iterate(cReal, cImag, maxIterations) {
  // We first calculate the magnitude |a+b1|
  let zReal = cReal;
  let zImag = cImag;
  let i = 0;
  // Then we iterate,
  // We will return if:
  //   i is larger than the maximum number of iterations.
  //   the magnitude of Z >= 2 (We're cutting out the sqrt for speed.)
  while (i++ < maxIterations) {
    // We'll only calculate the squares once.
    let zRealSq = zReal**2;
    let zImagSq = zImag**2;
    // Did we escape the mandelbrot set?
    if ((zRealSq + zImagSq) > 4) {
      break;
    }
    // otherwise calculate the next iteration.
    zImag = 2 * zReal * zImag + cImag;
    zReal = zRealSq - zImagSq + cReal;
  }
  return i;
}
export default iterate;