<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Mandel - Rendering the Mandelbrot and Julia sets</title>
    <meta name="description" content="Description of a program to render the Mandelbrot and Julia fractal sets using JavaScript, C++, SDL2 and OpenCl." />
    <link rel="canonical" href="https://dcol.nl/mandel" />
  </head>
  <body>
    <!-- Header -->
    <div class="jumbotron jumbotron-fluid" id="banner">
        <div class="container">
          <h1>Mandel</h1>
          <p class="lead text-muted">Infinite mathematics with the Mandelbrot and Julia fractal sets</p>
        </div>
    </div>
    <!-- Navigation -->
    {{menu}}
    <!-- Body -->
    <main class="container pb-5">
      <h2>Introduction</h2>
      <p>
        After watching a very interesting <a target="_blank" href="https://youtu.be/FFftmWSzgmk">
        video</a> about the mandelbrot set I decided to explore the topic programatically.
      </p>
      <!-- The Math -->
      <h2>The math</h2>
      <p>
        The Mandelbrot set is the set of values c in the complex plain for which
        the following iteration:
        <blockquote class="blockquote">
          <math><vec>z</vec><sub>n+1</sub> = z<sub>n</sub><sup>2</sup> + c</math>
        </blockquote>
        remains bounded when starting with <math>z<sub>0</sub>=0</math>.
        We know that once the equation 'escapes' past <math>|z| = 2</math> it
        will become unbounded. As such we can escape our iteration and mark
        the coordinates as outside the set when we surpass this limit.
        The rendered image is simply a color coding of this result.
        A maximum number of 255 iterations is made for each coordinate in the
        plane and the number of iterations required to reach
        <math>|z| = 2</math> is used to set the color of each pixel. Where 255
        is black and 0 is white yielding the following image:
      </p>
      <figure>
        <img src="../../assets/img/mandel/mandel_250.png" alt="A render of the Mandelbrot set." class="img-thumbnail">
        <figcaption class="figure-caption">A render of the Mandelbrot set.</figcaption>
      </figure>
      <!-- MandelJs -->
      <h2>MandelJs</h2>
      <p>
        At first I wrote a simple web application to render the mandelbrot set.
        This is the result of a weekend of mathematical exploration. I made an
        attempt to port the computation-heavy code to WASM for performance gains.
        After several hours of trying to get emscripten to play nice with
        webpack I decided to defer playing with it to another time and leave the
        code as-is in JS.
        This is a bit slow since for every pixel it will do a maximum of 255
        iterations to determine the stability of the mandelbrot function.
      </p>
      <p>
        You can have a play with it <a href="/mandeljs" rel="nofollow">here</a>.<br>
        To navigate click the canvas and:
        <ul>
          <li><b>W</b> move up.</li>
          <li><b>A</b> move left.</li>
          <li><b>S</b> move down.</li>
          <li><b>D</b> move right.</li>
          <li><b>Arrow up</b> zoom in.</li>
          <li><b>Arrow down</b> zoom out.</li>
        </ul>
        <i>Moving does not currently work in Edge.</i><br>
        <i>Resizing the window doesn't work without a reload.</i>
      </p>
      <!-- MandelCpp -->
      <h2>MandelCpp</h2>
      <p>
        After the disappointing performance of the Javascript implementation I
        decided to port the code to C++. I built on my SDLWindow class created
        for <a href="/woud">Woud</a> and quickly had a working application.
      </p>
      <p>
        Again, I was disappointed by the speed of the application. Requiring a
        maximum of 255 million iterations per frame for my 1000x1000 window and
        no room for improvement in the iteration itself I was looking for
        another way to increase performance. Using multi-threading wasn't going
        to cut it. The speed increase of 4x for a fairly standard CPU with the
        added overhead of multi-threading just wouldn't give significant
        results.
      </p>
      <h4>OpenCL</h4>
      <p>
        Enter
        <a href="https://www.khronos.org/opencl/" target="_blank">OpenCL</a>.
        <blockquote class="blockquote">
          "The open standard for parallel programming of heterogeneous systems."
        </blockquote>
        Since the problem at hand is the highly parallel processing of
        floating-point arithmetic it is perfectly suited to a solution in
        OpenCL. With a little help from the excellent book
        <a href="https://www.amazon.co.uk/Heterogeneous-Computing-OpenCL-David-Kaeli/dp/0128014148" target="_blank">
          Heterogeneous Computing with OpenCL 2.0
        </a>
        I ported the algorithm to OpenCL C. Passing in an array with every
        pixel's coordinates in the rendered space and then iterating over the
        Mandelbrot function for many points in parallel, I finally had an
        application that was blazing fast even on my basic GPU.
      </p>
      <figure class="figure">
        <video
          class="embed-responsive embed-responsive-1by1"
          poster="../../assets/img/mandel/mandelcpp_opencl.jpg"
          alt="A video comparing cpu rendering zooming in and OpenCL rendering zooming out."
          controls
        >
          <source src="../../assets/img/mandel/mandelcpp_opencl.webm" type="video/webm">
          <source src="../../assets/img/mandel/mandelcpp_opencl.mp4" type="video/mp4">
          Your browser does not support video.
        </video>
        <figcaption class="figure-caption">
          A comparison of CPU rendering zooming in and OpenCL rendering
          zooming out, both at maximum speed.
        </figcaption>
      </figure>
      <!-- Julia sets -->
      <h2>Julia sets</h2>
      <figure>
        <img src="../../assets/img/mandel/julia_250.png" alt="A render of a Julia set." class="img-thumbnail">
        <figcaption class="figure-caption">A render of a Julia set.</figcaption>
      </figure>
      <p>
        An interesting variation of the Mandelbrot set is the Julia set (or
        rather, the Mandelbrot set is a specific Julia set.)
        Where the mandelbrot set
        <math><vec>z</vec><sub>n+1</sub> = z<sub>n</sub><sup>2</sup> + c</math>
        starts with <math>z<sub>0</sub>=0</math>, a Julia set start with
        <math>z<sub>0</sub></math> being any coordinate in the complex plane. As
        such there is an endless number of Julia sets.
      </p>
      <p>
        This means that we can render a Julia set that corresponds to a
        coordinate in the complex plane that we have rendered the Mandelbrot set
        on. Doing this shows an interesting property of the Julia set: the shape
        of the set shows features similar to the corresponding area of the
        mandelbrot set.
      </p>
      <p>
        I modified the software to capture coordinates on mouse-click and
        convert them to coordinates in the imaginary plane in the rendered
        space. The original code rendering the Mandelbrot set was easily
        converted to run Julia sets, the same OpenCL kernel now calculates the
        Mandelbrot set with <math>z<sub>0</sub>=0</math> being passed in. The
        Julia set is rendered to a second window enabling the exploration of
        these sets.
      </p>
      <figure class="figure">
        <video
          class="embed-responsive embed-responsive-1by1"
          poster="../../assets/img/mandel/mandel_julia.jpg"
          alt="A video showing rendering of Julia sets corresponding to coordinates on the mandelbrot set."
          controls
        >
          <source src="../../assets/img/mandel/mandel_julia.webm" type="video/webm">
          <source src="../../assets/img/mandel/mandel_julia.mp4" type="video/mp4">
          Your browser does not support video.
        </video>
        <figcaption class="figure-caption">
          Rendering of Julia sets corresponding to coordinates on the mandelbrot set in MandelCpp.
        </figcaption>
      </figure>
      <!-- 3D -->
      <h2>Transformation to 3 Dimensions</h2>
      <p>
        Looking at the beautiful motion of the transforming Julia sets when
        moving the cursor across the imaginary plane I wrote some code to
        capture these images for each frame. I then loaded this series of images
        into the open-source scientific imaging package
        <a href="https://fiji.sc/" target="_blank">Fiji</a> mapping the changing coordinates to
        the Z-axis. After cleaning this model up in Blender I printed a Julia
        vase.
      </p>
      <figure>
        <img
          src="../../assets/img/mandel/juliaprint_358.jpg"
          alt="A 3D print of a Julia set transforming along the Z-axis.."
          class="img-thumbnail"
        >
        <figcaption class="figure-caption">
          A 3D print of a Julia set transforming along the Z-axis.
        </figcaption>
      </figure>
      <p>
        The path through the complex plane was captured by moving the cursor by
        hand leading to less than smooth transitions, and the tool-chain using
        Fiji was a bit cumbersome. There is interesting potential to design some
        beautiful looking vases or lamp shades in the future.
      </p>
    </main>
    <!-- Footer -->
    {{footer}}
  </body>
</html>