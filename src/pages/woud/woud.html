<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Woud - A cellular automata forest simulation in C++/SDL2</title>
    <meta name="description" content="Description of a cellular automata forest simulation in C++/SDL2." />
    <link rel="canonical" href="https://dcol.nl/woud" />
  </head>
  <body>
    <!-- Header -->
    <div class="jumbotron jumbotron-fluid" id="banner">
        <div class="container">
          <h1>Woud</h1>
          <p class="lead text-muted">A cellular automata flammable forest</p>
        </div>
    </div>
    <!-- Navigation -->
    {{menu}}
    <!-- Body -->
    <main class="container pb-5">
      <!-- Introduction -->
      <h2>Introduction</h2>
        <p>
          After reading about Conway's game of life I decided to try my hand at
          cellular automata. Building on the base SDL display built for
          <a href="/prime">Prime</a> I decided to simulate a forest where trees go
          through a life-cycle. They grow from a seed, they mature, they
          reproduce, they die and finally rot away. Occasionally, lightning
          strikes and a wildfire rampages through the woods (or woud in dutch).
        </p>
        <figure class="figure">
          <video
            class="embed-responsive embed-responsive-1by1"
            poster="../../assets/img/woud/woud_500.png"
            alt="Output of the Woud program. The cellular automata inspired forest simulating though life and death."
            controls
          >
            <source src="../../assets/img/woud/woud.webm" type="video/webm">
            <source src="../../assets/img/woud/woud.mp4" type="video/mp4">
            Your browser does not support video.
          </video>
          <figcaption class="figure-caption">
            Output of the Woud program. The cellular automata inspired forest
            simulating though life and death.
          </figcaption>
        </figure>
      <!-- Cellular automata -->
      <h2>Cellular Automata</h2>
        <p>
          A cellular automaton is placed somewhere on a grid of cells. The
          automaton has a finite number of states, and interacts with its direct
          neighbourhood. The lattice of cells goes though generations by mutating
          each cell according to some fixed rules. This mutates state of cells on
          the grid over time. The steps in time are called generations or cycles.
        <p>
        </p>
          What is so fascinating about cellular automata is that very complex
          behaviour emerges from very simple rules. When many very simple parts
          interact the macro-scale outcome is chaotic, and often beautiful. A
          good example is
          <a href="https://en.wikipedia.org/wiki/Rule_30" target="_blank">rule 30</a>
          from Wolfram's classification scheme.
        </p>
      <!-- The automaton -->
      <h2>The Automaton</h2>
        <!-- States -->
        <h3>States</h3>
          <p>
            The automaton in this simulation is the tree. It goes through 4 stages
            in it's natural life-cycle, each drawn a different colour:
            <ul>
              <li>
                <b>Growing</b> - <i>Light green</i> - The tree has spawned from a
                seed and is growing to maturity. It's size increases every
                generation.
              </li>
              <li>
                <b>Mature</b> - <i>Dark green</i> - The tree has reached is full
                hight. It is sexually mature and will go into a regular
                "bloom-cycle".
              </li>
              <li>
                <b>Blooming</b> - <i>Yellow</i> - The tree is reproducing. For the
                duration of this period the tree spawn new trees in random locations
                in a radius around it.
              </li>
              <li>
                <b>Dead</b> - <i>Brown</i> - The tree has died after going through a
                number of bloom cycles. It is slowly rotting away, decreasing it's
                size until it is removed from the scene.
              </li>
            </ul>
            One extra state a tree can find itself in is:
            <ul>
              <li>
                <b>Burning</b> - <i>Red</i> - The tree is on fire. It's size is 
                reduced every generation until it is removed from the scene. Whilst
                it is burning it may set fire to any other trees in a radius around
                it.
              </li>
            </ul>
          </p>
        <!-- Rules -->
        <h3>Rules</h3>
          <p>
            Every iteration of the simulation the function Tree::live() is called,
            moving the tree through it's states by following the following decision
            tree:
            <ul>
              <li>
                If the tree is growing it:
                <ul>
                  <li>Increases it's age and size by one every cycle.</li>
                  <li>If it reaches max age it progresses into mature.</li>
                </ul>
              </li>
              <li>
                If the tree is mature it:
                <ul>
                  <li>Increases it's age by one every cycle.</li>
                  <li>
                    If it reaches max age it:
                    <ul>
                      <li>Progresses into dead when it has reached it's maxBloomCycles.</li>
                      <li>Otherwise progresses into blooming.</li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li>
                If the tree is blooming it:
                <ul>
                  <li>Increases it's age by one every cycle.</li>
                  <li>Has a chance to spawn a new tree inside it's bloom-radius.</li>
                  <li>If it reaches max age it progresses into mature.</li>
                </ul>
              </li>
              <li>
                If the tree is dead it:
                <ul>
                  <li>Increases it's age and decreases it's size by one every cycle.</li>
                  <li>If it reaches max age it gets removed.</li>
                </ul>
              </li>
              <li>
                If the tree is burning it:
                <ul>
                  <li>Increases it's age and decreases it's size by ten every cycle.</li>
                  <li>Has a chance to set fire to any tree inside it's burn-radius.</li>
                  <li>If it reaches zero size it gets removed.</li>
                </ul>
              </li>
            </ul>
          </p>
      <h2>Scene</h2>
      <p>
        The scene is an important part of every simulation or game. It is where
        we store all the elements inside the simulation. It can be as simple as
        an array or more complex.Because the collision detection in this
        simulation is more complex than a simple next-door-neighbour a more
        complex data-structure was required. Collision detection is necessary:
        <ul>
          <li>To prevent two trees from spawning in the same place.</li>
          <li>To determine which trees get set on fire when lightning strikes.</li>
          <li>To determine which trees get set on fire when a close-by tree is burning.</li>
        </ul>
        This could be achieved in a way that one would traditionally use for
        cellular automata: by creating a 2-dimensional array that represents the
        world and storing the automata in this structure. Possibly with a
        secondary unidimensional array to make iteration more efficient (since
        the 2-dimensional array would be sparsely populated).
      </p>
      <p>  
        While this
        approach would be very efficient (retrieving object at set coordinates
        would be an O(1) operation) it also has drawbacks. Firstly the
        two-dimensional array would use up a large amount of memory even when
        mostly empty. This could be reduced by storing pointers to a secondary
        array where the actual data is stored, but even with that approach an
        empty 1000x1000 pixel forest would consume 8 Mb of memory (assuming 64
        bit pointers). Another drawback arises should we choose to use trees
        that are bigger than 1x1 pixel or have odd shapes. Lastly, it is just
        not very interesting. If we're building programs to learn new things and
        create art we may as well try new things, and visually interesting ones
        at that. As such, I decided to implement (and draw) a quad tree.
      </p>
      <h2>The Quadtree</h2>
      <p>
        The <a href="https://en.wikipedia.org/wiki/Quadtree" target="_blank">quadtree</a>
        is a tree data structure that uses a divide-and-conquer mechanism to
        spatially index objects. It works by dividing itself into four
        quadrants. Any object stored in the quad-tree that fits entirely in a
        quadrant is stored into that quadrant. If the quadrant gets too full it
        will split into another four quadrants, redistributing it's contents
        into the newly created sub-quadrants. Any object that doesn't fit
        entirely in the new sub-quadrants remains in the top one.
      </p>
      <p>
        This way retrieving any object that fits into a certain hit-box is
        relatively efficient. Instead of testing every single object in the
        scene you can quickly discard any object that is far outside the range
        of your area of interest.
      </p>
      <p>
        The quadtree is overlaid on the woud simulation in grey. Note how denser
        areas split up into more dub-divisions.
      </p>
    </main>
    <!-- Footer -->
    {{footer}}
  </body>
</html>