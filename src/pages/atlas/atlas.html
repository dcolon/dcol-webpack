<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Atlas - A kinetic art projects drawing lines in the sand.</title>
    <meta name="description" content="A kinetic art project utilising 3D-Printed parts to draw geometric patterns in sand." />
    <link rel="canonical" href="https://dcol.nl/atlas" />
  </head>
  <body>
    <!-- Header -->
    <div class="jumbotron jumbotron-fluid" id="banner">
        <div class="container">
          <h1>Atlas</h1>
          <p class="lead text-muted">Drawing a line in the sand</p>
        </div>
    </div>
    <!-- Navigation -->
    {{menu}}
    <!-- Body -->
    <main class="container pb-5">
      <!-- Introduction -->
      <h2>Introduction</h2>
      <p>
        I wanted to try my hand at robotics for the first time, and saw a video
        about an interesting project: the
        <a href="https://youtu.be/qdseS4xLioo">Sisyphus table</a>. It works by
        moving a metal ball through a layer of sand using a magnet underneath a
        plate. The magnet is being moved by some sort of mechanism. I thought it
        looked like a fun project to get my hands dirty with robotics and
        decided to design and build a similar sand-table, this time named after
        the titan holding up the world: Atlas.
      </p>
      <h2>Requirements</h2>
      <p>
        The system should meet the following requirements:
        <ul>
          <li>
            To keep cost of a prototype down:
            <ul>
              <li>use stepper motors rather than servos;</li>
              <li>use components often used in 3D printers (these are cheap);</li>
              <li>design the mechanism so that no rotating contact is required;</li>
              <li>use 3D-printed parts where possible.</li>
            </ul>
          </li>
          <li>
            To meet functional requirements, the system must:
            <ul>
              <li>
                have two degrees of freedom, being able to move
                a magnet on a plane bounded by a circle;
              </li>
              <li>be able to be contained beneath a flat circular plate;</li>
              <li>be wi-fi enabled to allow for control from an external device;</li>
              <li>be small enough to be operated on a table-top.</li>
            </ul>
          </li>
        </ul>  
      </p>
      <h2>Mechanical design</h2>
      <figure>
        <img src="../../assets/img/atlas/atlas_sketch_500.png" alt="A sketch of the initial design of the Atlas mechanism." class="img-thumbnail">
        <figcaption class="figure-caption">A sketch of the initial design of the Atlas mechanism.</figcaption>
      </figure>
      <p>
        From these requirements I decided on a base design. A stepper motor
        mounted on a base plate would rotate a lazy-susan/slew bearing. A second
        stepper motor would be mounted in the middle of this bearing, the wires
        routed through the hole of the bearing eliminating the need for a
        rotating contact at the cost of slightly more complex kinematics.
        A hollow tower is then mounted on the slew bearing with a diameter large
        enough to encompass the inner assembly. On top of this, an arm will be
        mounted with a rack that will be moved by a pinion gear on the inner
        stepper motor. To prevent unwanted extension or retraction of the arm
        the controller will rotate the inner pinion to compensate for rotation
        of the tower component. Two end-stops will be mounted on the inner tower
        to home the mechanism.
      </p>
      <figure>
        <img src="../../assets/img/atlas/tower_500.jpg" alt="A side view of the assembled tower mechanism." class="img-thumbnail">
        <figcaption class="figure-caption">A side view of the assembled tower mechanism.</figcaption>
      </figure>
      <!-- Electronic design -->
      <h2>Electronic design</h2>
      <p>
        To control the robot a number of components are required:
        <ul>
          <li>
            <b>Power supply</b> - As this is the only component working at a
            relatively high voltage I used a quality Meanwell power supply unit.
            I used a 12V - 3A power supply I had lying around but a higher
            voltage would work.
          </li>
          <li>
            <b>DC-DC converter</b> - A converter with sufficient range to step
            down the higher voltage used for the stepper motors to 5V for the
            electronics. I used a cheap LM2596.
          </li>
          <li>
            <b>Motor drivers</b> - To control the stepper motors A4988 motor
            drivers were used.
          </li>
          <li>
            <b>Controller</b> - The cheap wi-fi enabled ESP8266 was chosen as
            the main controller. Alternatively, a raspberry pi could be used,
            but this would be more expensive and less robust. An added benefit
            was that this means I get to practice embedded development.
          </li>
          <li>
            <b>End-stops</b> - "Opto" optical end-stops were chosen for being
            able to let a rotating components pass through them unhindered when
            not homing the device.
          </li>
          <li>
            <b>Stepper motors</b> - Nema 17 (17HS13-0404S1) motors were chosen,
            which are cheaply available due to their mass use in 3D printers.
          </li>
        </ul>
        These components were quite simply connected to each other. Small
        capacitors were put between the motor power supply Vin and GND, and a
        diode was placed on the positive in of the DC-DC converter to stop dirty
        power interfering with motor drivers. The stepper driver's step size,
        step and direction pins were connected to digital I/O pins on the
        controller as are the the two end-stop's outputs.
      </p>
      <figure>
        <img src="../../assets/img/atlas/comp_electronics_500.jpg" alt="Atlas' electronics on a prototype board." class="img-thumbnail">
        <figcaption class="figure-caption">
          Atlas' electronics on a prototype board. The end-stops not connected.
          FLTR: Controller, 2x motor driver, DC-DC converter.
        </figcaption>
      </figure>
      <!-- Manufacturing -->
      <h2>Manufacturing</h2>
      <p>
        After the rough design, the components were designed one-by-one. The
        parts are 3D printed allowing for rapid prototyping and iteration. Most
        components were not altered form their initial design.
        <ul>
          <li>
            <b>Table gear</b> - Attached to the slew bearing and used for the
            rotation of the table. A double-herringbone was chosen to create
            smooth motion without inducing lateral forces.
          </li>
          <li>
            <b>Table driver</b> - The gear driving the table gear at a 3:1 ratio
            and the support for the stepper motor.
          </li>
          <li>
            <b>Inner tower</b> - The inner tower holds up the stepper motor
            driving the arm, and the two end-stop sensors used in homing the
            mechanism.
          </li>
          <li>
            <b>Outer tower</b> - The tower that supports the arm that rotates
            around the inner tower. It is mounted atop the table. It also has a
            small protrusion that passes through the homing sensor on every 
            rotation.
          </li>
          <li>
            <b>Arm</b> - The rack that holds the magnet and is driven by the
            pinion atop the inner tower. A lip at the end allows for the
            mounting of a small arm that is designed to trigger the second
            end-stop when the arm is fully extended in the table's
            zero-position.
          </li>
          <li>
            <b>Loader</b> - A bearing pressing down on the rim of the table
            gear. Compensation for the poor quality of the bearing to reduce
            play in the assembly.
          </li>
          <li>
            <b>Supports</b> - Supports holding up the sand table.
          </li>
        </ul>
      </p>
      <div id="componentsCarousel" class="carousel slide border mb-3" data-ride="carousel" data-interval="false">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="../../assets/img/atlas/comp_table_500.jpg" alt="The table gear mounted on the bearing.">
            <div class="carousel-caption bg-dark">
              The table gear mounted on the bearing.
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="../../assets/img/atlas/comp_tabledriver_500.jpg" alt="he table driver assembly.">
            <div class="carousel-caption bg-dark">
              The table driver assembly.
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="../../assets/img/atlas/comp_armdriver_500.jpg" alt="The inner tower assembly with pinion and end-stops.">
            <div class="carousel-caption bg-dark">
              The inner tower assembly with pinion and end-stops.
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="../../assets/img/atlas/comp_tower_front_500.jpg" alt="The outer tower and arm guides pictured from the front.">
            <div class="carousel-caption bg-dark">
              The outer tower and arm guides pictured from the front.
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="../../assets/img/atlas/comp_tower_back_500.jpg" alt="The outer tower and arm guides pictured from the back.">
            <div class="carousel-caption bg-dark">
              The outer tower and arm guides pictured from the back.
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="../../assets/img/atlas/comp_arm_500.jpg" alt="The arm with mounted neodymium magnet.">
            <div class="carousel-caption bg-dark">
              The arm with mounted neodymium magnet.
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="../../assets/img/atlas/comp_loader_500.jpg" alt="The loader with mounted bearing.">
            <div class="carousel-caption bg-dark">
              The loader with mounted bearing.
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="../../assets/img/atlas/comp_support_500.jpg" alt="The support holding the sand table.">
            <div class="carousel-caption bg-dark">
              The support holding the sand table.
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="../../assets/img/atlas/comp_psu_500.jpg" alt="The power supply unit with switch and C13 socket.">
            <div class="carousel-caption bg-dark">
              The power supply unit with switch and C13 socket.
            </div>
          </div>
        </div>
        <div class="carousel-controls">
          <a class="carousel-control carousel-control-prev" href="#componentsCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control carousel-control-next" href="#componentsCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <!-- Assembly -->
      <h2>Assembly</h2>
      <p>
        The entire system is assembled on an old cutting-board, to guide the
        wires for the center tower underneath the bearing care must be taken
        to assemble in the right order.
      </p>
      <figure>
        <img src="../../assets/img/atlas/assembly_animation.gif" alt="A time-lapse of the assembly of the system." class="img-thumbnail" id="assembly-animation">
        <figcaption class="figure-caption">
          A time-lapse of the assembly of the system.
        </figcaption>
      </figure>
      <!-- Operation -->
      <h2>Operation</h2>
      <p>
        The controller has the task of receiving the desired position of the
        system and changing the system towards this desired state. It must also
        initialise the system to a known position to home the system.
      </p>
      <!-- Homing -->
      <h3>Homing</h3>
      <p>
        To home the system, the table is rotated until the end-stop registers
        the small fin mounted in the outer tower has triggered it. The table
        position is then set to 0. At this point the table rotates a fixed
        number of steps to a position where the arm is in line with the second
        end-stop. The arm is then extended until it is detected by the end stop.
        The arm position is then set to full extension and homing has completed.
      </p>

      <figure class="figure">
        <video
          class="embed-responsive embed-responsive-1by1"
          poster="../../assets/img/atlas/homing.jpg"
          alt="Atlas' homing procedure."
          controls
        >
          <source src="../../assets/img/atlas/homing.webm" type="video/webm">
          <source src="../../assets/img/atlas/homing.mp4" type="video/mp4">
          Your browser does not support video.
        </video>
        <figcaption class="figure-caption">
          Atlas' homing procedure.
        </figcaption>
      </figure>
      <!-- Control -->
      <h3>Control</h3>
      <p>
        After initialisation the system enters control mode.
      </p>
      <p>
        The current position of the table and arm are stored as integer
        numbers. These are between 0 and 1200 for the table (the stepper motor
        takes 400 half-steps per rotation, and this is transferred across a 1:3
        gear ratio) and 2400 for the arm.
        The two are independently controlled.
      </p>
      <p>
        A simple web-page is served from the controller's built-in web-server.
        A circle representing the table is drawn on an html canvas, and a
        websocket connection is established to the websocket server running on
        the controller. When a click or touch is registered the angle and arm
        length between the touch (or click) and the center point are calculated
        using simple trigonometry, and then translated to coordinates Atlas
        understands. A message is then sent over the websocket connection
        containing these coordinates. When the main control loop on the
        controller services the websocket server, all messages but the last are
        discarded, and the last coordinates are copied into the desired
        position.
      </p>
      <p>
        The system's main control loop continuously compares the desired
        position of the table and the arm with their actual positions, and takes
        steps to unite the latter with the former.
      </p>
      <p>
        First the table position in evaluated. A calculation is done to
        determine whether the desired position is less than half the maximum
        rotation away in the clockwise and counter-clockwise direction.
        The system then takes a step in the direction that will get it to the
        desired position the quickest.
        If the table rotated clockwise and is now beyond the maximum position,
        the position is reset to zero (i.e. from 360 degrees to 0). If it is
        rotating counter-clockwise and is now 0 it is reset to the maximum (i.e.
        from 0 to 360 degrees). Finally, because the rotation of the table
        causes the arm to retract or extend, the arm position is increased or
        decreased by one.
      </p>
      <p>
        Then the arm is moved. Where the table is moved at a ratio of 1:3, the
        arm is directly driven. That means that one step on the table moves the
        arm as much as 3 steps of the pinion. As such, the arm pinion is always
        rotated by 3 steps.
      </p>
      <p>
        First the position of the arm is compared to the desired position. If
        the difference is at least three (otherwise it may move back-and-forth
        endlessly to get to a position the resolution of the pinion can't
        resolve). If the desired position is greater than that of the arm it is
        extended by three steps. Otherwise it is retracted.
      </p>
      <figure class="figure">
        <video
          class="embed-responsive embed-responsive-1by1"
          poster="../../assets/img/atlas/ball_demo.jpg"
          alt="Atlas moving a metal ball around the plate guided by user input."
          controls
        >
          <source src="../../assets/img/atlas/ball_demo.webm" type="video/webm">
          <source src="../../assets/img/atlas/ball_demo.mp4" type="video/mp4">
          Your browser does not support video.
        </video>
        <figcaption class="figure-caption">
          Atlas moving a metal ball around the plate guided by user input.
          Sent websocket messages are displayed in the bottom console on the
          screen.
        </figcaption>
      </figure>
      <!-- Next steps -->
      <h2>Next steps</h2>
      <p>
        The next steps in the project are to create a barrier on top of the
        circular plate to contain the sand. Then a means of drawing patterns
        will be developed. This will either be settings functions for the table
        and arm to follow, or a program to upload to the controller for it to
        play. Finally an LED-strip will be mounted on the inner edge of the
        table to highlight the patterns drawn in the sand. The brightness will
        be controlled using a PWM signal from the controller (via a MOSFET).
      </p>
      <p>
        The table driver may be redesigned to use a higher quality slew bearing
        as the current one causes a lot of noise from rattling around due to the
        large amount of play in the bearing. The loaders have reduced this noise
        slightly, but the system is still very loud. The extension and
        retraction of the arm is quiet in comparison.
      </p>
      <p>
        I will update this page as I progress on the project.
      </p>
    </main>
    <!-- Footer -->
    {{footer}}
  </body>
</html>