# DCol

**Daniel Colon - 16-04-2020**

My personal website, like they used to exist, but with fewer animated gifs.

https://dcol.nl

Built using Webpack boilerplate by
[lifenautjoe](https://github.com/lifenautjoe/webpack-starter-basic)
With a little help from:
[ericzhao](https://github.com/erickzhao/static-html-webpack-boilerplate/blob/master/webpack.config.prod.js)

## Philosophy

When building a website the default route is to use a CMS, be it Wordpress,
Drupal, Joomla etc. These are all great to hand over to the user in order to
give them control of their own site. They are also highly extendable should you
want to add some extra functionality.

The downside to these is that they are overkill for a simple website. They are
hard to maintain, have a considerable attack surface (and a resulting continuous
stream of vulnerabilities being disclosed.) and are, without good caching, slow.

What I wanted for my website was the web equivalent of a compiled application.
I wanted to have some flexibility in writing pages for the site without having
to copy paste boilerplate, or modifying 16 different files on a small change.
But I also wanted it to be fully static.

Using Webpack with a little bit of custom scripting this goal was attained.
With a little more work this could be further streamlined into an easy-to-use
compiled web template system. Maybe that is something to do for the future, but
for now this suits my needs.

## How does it work?

All the heavy lifting is done by [Webpack](https://webpack.js.org).
The two main components we use to process the HTML are the
[html-loader](https://webpack.js.org/loaders/html-loader/) and
[HtmlWebpackPlugin](https://webpack.js.org/plugins/html-webpack-plugin/)

### html-loader

The html-loader is run on our html files before they are handed over to
HtmlWebpackPlugin. The main purpose of the html-loader is to inject html shared
between pages using the
[preprocessor function](https://webpack.js.org/loaders/html-loader/#preprocessor):

It looks for the `{{menu}}` and `{{footer}}` tags and replaces them with the
file contents of `menu.html` and `footer.html`. The menu items are modified
to set the correct page to active.

```js
(content, loaderContext) => {
  // Load the menu and footer Html from their respective files.
  let menuHtml = fs.readFileSync('./src/menu.html', "utf8");
  let footerHtml = fs.readFileSync('./src/footer.html', "utf8");
  // Activate correct menu item in the loaded menu html.
  // At this point it's either the home page or the
  // projects (they're all in a sub-menu together.)
  const resource = path.basename(loaderContext.resource, '.html');
  if (resource === 'index') {
      menuHtml = menuHtml.replace('{{homeactive}}', 'active');
      menuHtml = menuHtml.replace('{{projectsactive}}', '');
  }
  else {
      menuHtml = menuHtml.replace('{{homeactive}}', '');
      menuHtml = menuHtml.replace('{{projectsactive}}', 'active');
  }
  // Set year in footer.
  footerHtml = footerHtml.replace('{{year}}', new Date().getFullYear()); 
  // Then replace menu and footer tags in page html.
  let result = content.replace('{{menu}}', menuHtml);
  result     = result.replace('{{footer}}', footerHtml);
  return result;
}
```

This gets the base Html of the page ready to hand over to the HtmlWebpackPlugin.

### HtmlWebpackPlugin

To bundle up all our individual pages and anything they need we use the
HtmlWebpackPlugin. An entry is pushed into the webpack module.exports for every
page folder in the pages directory by calling the `generateHTMLPlugins()`
function. This iterates over the pages directory and returns an entry for every
folder:
`glob.sync('./src/pages/**/*.html').map`

It then builds a HtmlWebpackPlugin entry specific to that page, doing a few
things:

#### noshared

```js
  // Don't include shared if we have a .noshared file in the page folder.
  const chunks = fs.existsSync(path.dirname(dir) + '/.noshared') ? [path.basename(dir, '.html')] : ['shared', path.basename(dir, '.html')]
```

It looks for a `.noshared` file in the page folder. If it exists it won't add the
shared chunk (`src/index.js` containing bootstrap.) This allows us to have a
page that doesn't use bootstrap such as the
[mandeljs demo](https://dcol.nl/mandeljs).

#### filename

The filename for the compiled html file is set to the name of the folder, the
end result is an html file for each page:

```
mandeljs.html
candlebear.html
index.html
prime.html
woud.html
mandel.html
atlas.html
```

## Building

First, make sure npm (or yarn, but we'll assume npm) is installed on your
machine. Then install the package dependencies using:

```sh
  npm install
```

To run the site on a local test server:

```sh
  npm start
```

To build for production:

```sh
  npm run build
```

To preview the production build

```sh
  npm run preview
```

## Video conversion

To convert video to mp4/webm and jpeg use ffmpeg:

```sh
# Jpeg image from frame 1
ffmpeg -i "input.mp4" -vframes 1 -vf scale=504:-2,crop=500:500:2:0 -q:v 1 "output.jpg"
# MP4 resized to 500 width at same aspect ration (500:-2).
ffmpeg -ss <start seconds> -t <end seconds> -i "input.mp4" -c:v libx264 -pix_fmt yuv420p -profile:v baseline -level 3.0 -crf 22 -preset veryslow -vf scale=500:-2 -an -movflags +faststart -threads 0 "output.mp4"
# WEBM with same options.
ffmpeg -ss <start seconds> -t <end seconds> -i "input.mp4" -c:v libvpx -qmin 0 -qmax 25 -crf 4 -b:v 1M -vf scale=500:-2 -an -threads 0 "output.webm"
# To crop video replace:
-vf scale=500:-2
# with:
-vf scale=500:-2,crop=500:500:2:0
# Where crop options are:
# crop=out_w:out_h:x:y
# out_w is the width of the output rectangle
# out_h is the height of the output rectangle
# x and y specify the top left corner of the output rectangle
```
