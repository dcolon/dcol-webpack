const path = require('path');
const glob = require('glob');
const fs   = require('fs')

const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin'); //installed via npm
const HTMLWebpackPlugin = require('html-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const buildPath = path.resolve(__dirname, 'dist');

const generateHTMLPlugins = () => glob.sync('./src/pages/**/*.html').map(
    dir => {
        // Don't include shared if we have a .noshared file in the page folder.
        const chunks = fs.existsSync(path.dirname(dir) + '/.noshared') ? [path.basename(dir, '.html')] : ['shared', path.basename(dir, '.html')]
        // Force script injection into the head for index. let the module
        // determine for all toher pages.
        let inject   = path.basename(dir, '.html') === 'index' ? 'head' : 'true';
        // Rewrite all files to index.html in their folder except for index.
        let filename = path.basename(dir);
        return new HTMLWebpackPlugin({
            inject: inject,
            filename: filename,
            // This will ensure we use html-loader.
            template: dir, // Input
            chunks: chunks,
            minify: {
                collapseWhitespace: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true
            },
        })
    }
);

// Generate a webpack entrypoint for every page in our pages directory.
// This will build a chunk for every page with the name of the directory.
const getEntries = () => {
    let result = {shared: './src/index.js'};
    const pages = glob.sync('./src/pages/*');
    pages.forEach(page => {
        if (fs.existsSync(page + '/index.js')) {
            result[path.basename(page)] = page + '/index.js';
        }
    });
    return result;
}

module.exports = {
    mode: 'production',
    devtool: false,
    entry: getEntries(),
    output: {
        filename: '[name].[hash:20].js',
        path: buildPath
    },
    node: {
        fs: 'empty'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env'],
                    plugins: [
                        ["prismjs", {
                            "languages": ["opencl"],
                            "plugins": ["line-numbers"],
                            "theme": "default",
                            "css": true
                        }]
                    ],
                }
            },
            {
                test: /\.html$/,
                loader: 'html-loader',
                options: {
                    attributes: true, // Enable processing of all default elements and attributes.
                    minimize: false, // We'll minify later in HTMLWebpackPlugin.
                    preprocessor: (content, loaderContext) => {
                        // Load the menu and footer Html from their respective files.
                        let menuHtml = fs.readFileSync('./src/menu.html', "utf8");
                        let footerHtml = fs.readFileSync('./src/footer.html', "utf8");
                        // Activate correct menu item in the loaded menu html.
                        // At this point it's either the home page or the
                        // projects (they're all in a sub-menu together.)
                        const resource = path.basename(loaderContext.resource, '.html');
                        if (resource === 'index') {
                            menuHtml = menuHtml.replace('{{homeactive}}', 'active');
                            menuHtml = menuHtml.replace('{{projectsactive}}', '');
                        }
                        else {
                            menuHtml = menuHtml.replace('{{homeactive}}', '');
                            menuHtml = menuHtml.replace('{{projectsactive}}', 'active');
                        }
                        // Set year in footer.
                        footerHtml = footerHtml.replace('{{year}}', new Date().getFullYear()); 
                        // Then replace menu and footer tags in page html.
                        let result = content.replace('{{menu}}', menuHtml);
                        result     = result.replace('{{footer}}', footerHtml);
                        return result;
                    },
                }
            },
            {
                test: /\.(scss|css|sass)$/,
                use: [
                    {
                        // creates style nodes from JS strings
                        loader: "style-loader",
                    },
                    {
                        // translates CSS into CommonJS
                        loader: "css-loader",
                    },
                    {
                        // compiles Sass to CSS
                        loader: "sass-loader",
                    }
                ]
            },
            {
                // Load all images as base64 encoding if they are smaller than 8192 bytes
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: '[name].[hash:20].[ext]',
                            limit: 8192
                        }
                    }
                ]
            },        
            {
                // Load all videos.
                test: /\.(eot|woff|woff2|svg|ttf|mp4|webm)([\?]?.*)$/,
                use: [
                    {
                        loader: 'file-loader',
                    }
                ]
            }
        ]
    },
    plugins: [
        //new HTMLWebpackPlugin({
        //    template: './index.html',
        //    // Inject the js bundle at the end of the body of the given template
        //    inject: 'body',
        //    minify: true,
        //}),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [buildPath],
        }),
        ...generateHTMLPlugins(),
        new FaviconsWebpackPlugin({
            // Your source logo
            logo: './src/assets/icon.png',
            // The prefix for all image files (might be a folder or a name)
            prefix: 'icons-[hash]/',
            // Generate a cache file with control hashes and
            // don't rebuild the favicons until those hashes change
            persistentCache: true,
            // Inject the html into the html-webpack-plugin
            inject: true,
            // favicon background color (see https://github.com/haydenbleasel/favicons#usage)
            background: '#fff',
            // favicon app title (see https://github.com/haydenbleasel/favicons#usage)
            title: 'dcol',

            // which icons should be generated (see https://github.com/haydenbleasel/favicons#usage)
            icons: {
                android: true,
                appleIcon: true,
                appleStartup: true,
                coast: false,
                favicons: true,
                firefox: true,
                opengraph: false,
                twitter: false,
                yandex: false,
                windows: false
            }
        }),
        new OptimizeCssAssetsPlugin({
            cssProcessor: require('cssnano'),
            cssProcessorOptions: {
                map: false,
                discardComments: {
                    removeAll: true
                },
                discardUnused: false
            },
            canPrint: true
        }),
    ]
};
